import os
import pickle

from experiments.evaluation.evaluation import (ensemble_evaluation,
                                               single_model_evaluation)
from experiments.evaluation.utils import ensemble_inference


def ring_experiment(generator, feats, targets, rings_ids, scaler, ensemble_size=4,
                    exp_name='ring_test', save_path=None, verbose=True):

    preds, preds2, single_model_preds = ensemble_inference(
        generator, feats, ensemble_size)

    if verbose:
        print(f"Validation set size is {feats.shape[0]}")
        print(f"Total number of rings is {len(rings_ids)}")

    res = {
        'model_scores': [],
        'emsemble_scores': []
    }
    for idx, ring in enumerate(rings_ids):
        ring_feats = feats[ring]
        ring_targets = targets[ring]
        ring_preds = [pred[ring] for pred in preds]
        ring_preds2 = [pred[ring] for pred in preds2]
        ring_model_preds = [pred[ring] for pred in single_model_preds]

        # Single model evaluation
        def model_eval(pred): return single_model_evaluation(
            ring_feats, ring_targets, pred, scaler)

        model_scores = [model_eval(pred) for pred in ring_model_preds]
        model_scores = [score.mean(axis=0)[0][1] for score in model_scores]

        # Ensemble evaluation
        ensemble_scores = ensemble_evaluation(
            ring_feats, ring_preds, ring_preds2, scaler)
        ensemble_scores = [score.mean(axis=0)[0][1]
                           for score in ensemble_scores]

        res['model_scores'].append(model_scores)
        res['emsemble_scores'].append(ensemble_scores)

        if verbose:
            print(f"Ring {idx} size is {ring.shape[0]}")
            print(f"Model scores: {model_scores}")
            print(f"Ensemble scores: {ensemble_scores}")

    if save_path is not None:
        path = os.path.join(save_path, f'{exp_name}.pickle')

        with open(path, 'wb') as handle:
            pickle.dump(res, handle)

    return res
