import numpy as np
import tensorflow as tf


def compute_uncertainty(
    data,
    ens_unc_model: tf.keras.Model,
    ref_unc_model: tf.keras.Model
):

    ens_unc = ens_unc_model(data).numpy().squeeze()
    ref_unc = ref_unc_model(data).numpy().squeeze()

    uncertainty = np.maximum(0, np.square(ens_unc) /
                             2 - np.square(ref_unc) / 2)
    uncertainty = np.sqrt(uncertainty)

    return uncertainty


def efficiency_momentum_with_uncertainty(
    momentum: np.ndarray,
    target: np.ndarray,
    feats: np.ndarray,
    ens_unc_model: tf.keras.Model,
    ref_unc_model: tf.keras.Model,
    threshold: float,
    out_feature_idx: int,
    num_boundaries=35,
):

    def compute_efficiency(y):
        return np.count_nonzero(y > threshold) / len(y)

    def compute_error(y):
        err = np.count_nonzero(y > threshold) * np.count_nonzero(y < threshold)
        err /= (len(y) ** 3)

        return np.sqrt(err)

    def compute_eff_bounds(y, uncertainty):
        unc = uncertainty[:, out_feature_idx]

        eff_l = np.count_nonzero(y + unc > threshold) / len(y)
        eff_h = np.count_nonzero(y - unc > threshold) / len(y)

        return eff_l, eff_h

    boundaries = np.linspace(momentum.min(), momentum.max(), num_boundaries)

    efficiency = []
    eff_low = []
    eff_high = []
    errors = []
    xs = []
    for xmin, xmax in zip(boundaries[:-1], boundaries[1:]):
        mask = (momentum > xmin) & (momentum < xmax)
        y = target[mask]
        X = feats[mask]

        if len(y) > 1:
            eff = compute_efficiency(y)
            err = compute_error(y)

            uncertainty = compute_uncertainty(X, ens_unc_model, ref_unc_model)

            eff_l, eff_h = compute_eff_bounds(y, uncertainty)

            efficiency.append(eff)
            eff_low.append(eff_l)
            eff_high.append(eff_h)
            errors.append(err)

            xs.append(0.5 * (xmin + xmax))

    return xs, efficiency, eff_low, eff_high, errors


def efficiency_bands_with_uncertainty(
    targets: np.ndarray,
    features: np.ndarray,
    ens_unc_model: tf.keras.Model,
    ref_unc_model: tf.keras.Model,
    threshold: float,
    out_feature_idx: int
):

    def compute_efficiency(y):
        return np.count_nonzero(y > threshold) / len(y)

    def compute_error(y):
        err = np.count_nonzero(y > threshold) * np.count_nonzero(y < threshold)
        err /= (len(y) ** 3)

        return np.sqrt(err)

    def compute_eff_bounds(y, uncertainty):
        unc = uncertainty[:, out_feature_idx]

        eff_l = np.count_nonzero(y + unc > threshold) / len(y)
        eff_h = np.count_nonzero(y - unc > threshold) / len(y)

        return eff_l, eff_h

    efficiency = []
    eff_low = []
    eff_high = []
    errors = []

    for feat, target in zip(features, targets):
        eff = compute_efficiency(target[:, out_feature_idx])
        err = compute_error(target[:, out_feature_idx])

        uncertainty = compute_uncertainty(feat, ens_unc_model, ref_unc_model)
        eff_l, eff_h = compute_eff_bounds(
            target[:, out_feature_idx], uncertainty)

        efficiency.append(eff)
        eff_low.append(eff_l)
        eff_high.append(eff_h)
        errors.append(err)

    x = np.arange(len(targets))

    return x, efficiency, eff_low, eff_high, errors
