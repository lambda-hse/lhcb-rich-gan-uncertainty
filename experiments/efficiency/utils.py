from tqdm import trange
import numpy as np

# from src.models.gans.generators.fcn_gen import VirtualEnsembleModel


def tf_to_numpy_dataset(dataset):
    feats = []
    targets = []

    for feat, target, _ in dataset:
        feat_ = feat.numpy()
        target_ = target.numpy()

        feats.append(feat_)
        targets.append(target_)

    feats = np.concatenate(feats)
    targets = np.concatenate(targets)

    return feats, targets


def threshold_selection(feature, n_tracks_fraction=0.9):
    if n_tracks_fraction < 0 or n_tracks_fraction > 1:
        raise ValueError("n_tracks_fraction should be in [0, 1] range")

    q = 100. * (1 - n_tracks_fraction)

    return np.percentile(feature, q)


def ensemble_and_ref_model_inference(generator, feats, ensemble_size=4):
    ensemble_preds = []
    single_model_preds = []

    generator.ensemble_inference_mode()
    for _ in trange(2 * ensemble_size):
        # target_gen = generator(feats, update_drop=True, elementwise=False)
        # ensemble_preds.append(target_gen.numpy())

        ensemble_preds.append(generator(feats).numpy())

    # generator(feats, update_drop=True, elementwise=True)

    generator.predict_mode()
    for _ in trange(2 * ensemble_size):
        # target_gen_elwise = generator(
        #     feats, update_drop=False, elementwise=True)
        # single_model_preds.append(target_gen_elwise.numpy())

        single_model_preds.append(generator(feats).numpy())

    return ensemble_preds, single_model_preds


# def ensemble_and_ref_model_inference_on_bands(generator: VirtualEnsembleModel, bands, ensemble_size=4):
#     ensemble_preds = []
#     single_model_preds = []

#     for _ in trange(ensemble_size):
#         generator(bands[0][0]) #, update_drop=True, elementwise=False)

#         bands_preds = []
#         for band in bands:
#             feats, _ = band

#             target_gen = generator(feats) #, update_drop=False, elementwise=False)
#             bands_preds.append(target_gen.numpy())
        
#         ensemble_preds.append(bands_preds)

#     generator(bands[0][0]) #, update_drop=True, elementwise=True)
#     for band in bands:
#         feats, _ = band

#         target_gen_elwise = generator(
#             feats) #, update_drop=False, elementwise=True)
#         single_model_preds.append(target_gen_elwise.numpy())

#     return ensemble_preds, single_model_preds


def ensemble_and_ref_model_inference_on_bands(generator, bands, ensemble_size=4):
    ensemble_preds = []
    single_model_preds = []

    for _ in trange(ensemble_size):
        generator.single_model_inference_mode()

        bands_preds = []
        for (feats, _) in bands:
            bands_preds.append(generator(feats).numpy()) 
        ensemble_preds.append(bands_preds)

    generator.predict_mode()
    for (feats, _) in bands:
        single_model_preds.append(generator(feats).numpy())

    return ensemble_preds, single_model_preds


def efficiency_momentum(momentum, target, threshold, num_boundaries=35):
    def compute_efficiency(y):
        return np.count_nonzero(y > threshold) / len(y)

    def compute_error(y):
        err = np.count_nonzero(y > threshold) * np.count_nonzero(y < threshold)
        err /= (len(y) ** 3)

        return np.sqrt(err)

    boundaries = np.linspace(momentum.min(), momentum.max(), num_boundaries)

    efficiency = []
    errors = []
    xs = []
    for xmin, xmax in zip(boundaries[:-1], boundaries[1:]):
        mask = (momentum > xmin) & (momentum < xmax)
        y = target[mask]

        if len(y) > 1:
            eff = compute_efficiency(y)
            err = compute_error(y)
            
            efficiency.append(eff)
            errors.append(err)

            xs.append(0.5 * (xmin + xmax))
            
    return xs, efficiency, errors


def efficiency_bands(targets, threshold, idx):
    def compute_efficiency(y):
        return np.count_nonzero(y > threshold) / len(y)

    def compute_error(y):
        err = np.count_nonzero(y > threshold) * np.count_nonzero(y < threshold)
        err /= (len(y) ** 3)

        return np.sqrt(err)

    efficiency = []
    errors = []

    for target in targets:
        eff = compute_efficiency(target[:, idx])
        err = compute_error(target[:, idx])

        efficiency.append(eff)
        errors.append(err)

    x = np.arange(len(targets))

    return x, efficiency, errors
