import tensorflow as tf
from tensorflow.keras.layers import (BatchNormalization, Dense, InputLayer,
                                     LeakyReLU)


def uncertainty_mlp():
    return tf.keras.Sequential([
        InputLayer(3,),
        Dense(128, activation=LeakyReLU(0.1)),
        # BatchNormalization(),
        Dense(128, activation=LeakyReLU(0.1)),
        # BatchNormalization(),
        Dense(128, activation=LeakyReLU(0.1)),
        Dense(5)
    ])
