from tqdm import trange


def ensemble_inference(generator, feats, ensemble_size=4):
    preds = []
    preds2 = []
    single_model_preds = []

    for _ in trange(ensemble_size):
        target_gen = generator(feats, update_drop=True, elementwise=False)
        target_gen2 = generator(feats, update_drop=False, elementwise=False)

        target_gen_elwise = generator(feats, update_drop=True, elementwise=True)

        preds.append(target_gen.numpy())
        preds2.append(target_gen2.numpy())
        single_model_preds.append(target_gen_elwise.numpy())

    return preds, preds2, single_model_preds
