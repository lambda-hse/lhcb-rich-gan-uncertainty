import tensorflow as tf

from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input


def RICHDiscriminator(feat_input_shape=(3,), target_input_shape=(5,),
                      num_layers=5, last_layer_dims=256):
    feat_input = Input(shape=feat_input_shape)
    target_input = Input(shape=target_input_shape)

    inputs = tf.keras.layers.concatenate([feat_input, target_input])

    layers = tf.keras.Sequential(
        [Dense(128, activation=LeakyReLU(0.05)) for _ in range(num_layers)]
    )
    layers.add(Dense(last_layer_dims))

    outputs = layers(inputs)

    model = tf.keras.Model(inputs=[feat_input, target_input], outputs=outputs)

    return model
