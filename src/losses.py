import tensorflow as tf


def cramer_critic(disc_input1, disc_input2):
    return tf.norm(disc_input1 - disc_input2, axis=1) - tf.norm(disc_input1, axis=1)


def cramer_gen_loss(disc_real, disc_gen1, disc_gen2, weights):
    w_real, w_gen1, w_gen2 = weights

    loss = cramer_critic(disc_real, disc_gen2) * w_real * w_gen2 - \
        cramer_critic(disc_gen1, disc_gen2) * w_gen1 * w_gen2

    return tf.reduce_mean(loss)


def gradient_loss(interpolates, disc_interp, disc_gen):
    disc_interpolates = cramer_critic(disc_interp, disc_gen)
    gradients = tf.gradients(disc_interpolates, interpolates)
    gradients = tf.concat(gradients, axis=1)

    slopes = tf.norm(tf.reshape(
        gradients, [tf.shape(gradients)[0], -1]), axis=1)
    gradient_penalty = tf.reduce_mean(
        tf.square(tf.maximum(tf.abs(slopes) - 1, 0)))

    return gradient_penalty


def cramer_critic_loss(lambd, gradient_loss, gen_loss):
    return lambd * gradient_loss - gen_loss
