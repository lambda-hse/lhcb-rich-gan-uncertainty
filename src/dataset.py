import tensorflow as tf

from src.datasets.utils_rich import parse_example


class CramerGANDataset:
    AUTOTUNE = tf.data.experimental.AUTOTUNE

    def __init__(self, data_np, num_instances=3, iter_batch_size=16,
                 batch_size=int(1e3), shuffle=True, shuffle_buffer=int(1e5), ds_size=None):

        self.shuffle_buffer = shuffle_buffer

        # TODO: update quick fix
        total_batch = iter_batch_size * batch_size
        size_cut = data_np.shape[0] // total_batch
        size_cut *= total_batch
        data_np = data_np[:size_cut]

        datasets = tuple(self._create_dataset(data_np, shuffle)
                         for _ in range(num_instances))
        dataset = datasets[0] if num_instances == 1 else tf.data.Dataset.zip(
            datasets)

        dataset = dataset.batch(batch_size)

        if iter_batch_size > 1:
            dataset = dataset.batch(iter_batch_size)

        self.dataset = dataset

        self.batch_size = batch_size
        self.iter_batch_size = iter_batch_size
        self.total_batch = total_batch
        self.ds_size = ds_size if ds_size is not None else data_np.shape[0]

    def _create_dataset(self, data, shuffle):
        dataset = tf.data.Dataset.from_tensor_slices(data)
        dataset = dataset.map(parse_example, num_parallel_calls=self.AUTOTUNE)

        if shuffle:
            dataset = dataset.shuffle(self.shuffle_buffer)

        return dataset
