import datetime
import os
import time

import tensorflow as tf
from tqdm import tqdm

from src.dataset import CramerGANDataset
from src.losses import cramer_critic_loss, cramer_gen_loss, gradient_loss


class CramerGANTrainer:
    def __init__(
            self,
            generator: tf.keras.Model,
            discriminator: tf.keras.Model,
            generator_optimizer: tf.keras.optimizers.Optimizer,
            discriminator_optimizer: tf.keras.optimizers.Optimizer,
            checkpoint_dir: str,
            reg_loss_lambda: float = 1e-3,
            log_dir: str = 'tmp') -> None:

        self.generator = generator
        self.discriminator = discriminator
        self.generator_optimizer = generator_optimizer
        self.discriminator_optimizer = discriminator_optimizer

        self.reg_loss_lambda = reg_loss_lambda

        print(self.reg_loss_lambda)

        self.checkpoint_dir = checkpoint_dir
        self.checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
        self.checkpoint = tf.train.Checkpoint(generator_optimizer=self.generator_optimizer,
                                              discriminator_optimizer=self.discriminator_optimizer,
                                              generator=self.generator,
                                              discriminator=self.discriminator)

        self.summary_writer = tf.summary.create_file_writer(
            log_dir + "fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))

    @tf.function
    def train_step_critic(self, step, features, targets, weights, lambd=15.):
        features_real, features1, features2 = features
        targets_real, _, _ = targets

        with tf.GradientTape() as disc_tape:
            # Generator inference on two data points
            gen_output1 = self.generator(features1, training=True)
            gen_output2 = self.generator(features2, training=True)

            # Discriminator inference on fake and real data
            disc_gen1 = self.discriminator(
                [features1, gen_output1], training=True)
            disc_gen2 = self.discriminator(
                [features2, gen_output2], training=True)
            disc_real = self.discriminator(
                [features_real, targets_real], training=True)

            # Computing preliminaries for gradient loss
            alpha = tf.random.uniform(
                shape=[tf.shape(features_real)[0], 1], minval=0., maxval=1.)

            interp_features = alpha * features_real + (1. - alpha) * features1
            interp_targets = alpha * targets_real + (1. - alpha) * gen_output1

            # tf.concat([interp_features, interp_targets], axis=1)
            interpolates = [interp_features, interp_targets]
            disc_interp = self.discriminator(
                [interp_features, interp_targets], training=True)

            # Gradient and generator losses
            grad_loss = gradient_loss(interpolates, disc_interp, disc_gen2)
            gen_loss = cramer_gen_loss(disc_real, disc_gen1, disc_gen2, weights)

            critic_loss = cramer_critic_loss(lambd, grad_loss, gen_loss)

        critic_gradients = disc_tape.gradient(
            critic_loss, self.discriminator.trainable_variables)
        self.discriminator_optimizer.apply_gradients(
            zip(critic_gradients, self.discriminator.trainable_variables))

        with self.summary_writer.as_default():
            tf.summary.scalar('critic_loss', critic_loss, step=step)

        return critic_loss

    @tf.function
    def train_step_generator(self, step, features, targets, weights):
        features_real, features1, features2 = features
        targets_real, _, _ = targets

        with tf.GradientTape() as gen_tape:
            # Generator inference on two data points
            gen_output1 = self.generator(features1, training=True)
            gen_output2 = self.generator(features2, training=True)

            # Discriminator inference of fake and real data
            disc_gen1 = self.discriminator(
                [features1, gen_output1], training=True)
            disc_gen2 = self.discriminator(
                [features2, gen_output2], training=True)
            disc_real = self.discriminator(
                [features_real, targets_real], training=True)

            # Generator loss
            gen_loss = cramer_gen_loss(disc_real, disc_gen1, disc_gen2, weights)

            # Reg loss
            reg_loss = 0 # tf.add_n([loss for loss in self.generator.losses])
            loss = gen_loss + self.reg_loss_lambda * reg_loss

        gen_gradients = gen_tape.gradient(
            loss, self.generator.trainable_variables)
        self.generator_optimizer.apply_gradients(
            zip(gen_gradients, self.generator.trainable_variables))

        with self.summary_writer.as_default():
            tf.summary.scalar('gen_loss', gen_loss, step=step)
            tf.summary.scalar('reg_loss', reg_loss, step=step)

        return gen_loss, reg_loss

    # @tf.function
    # def train_step_dropout_reg(self, step):


    def evaluate(self, test_ds):
        pass

    def fit_cramer(
            self,
            epochs: int,
            train_ds: CramerGANDataset,
            test_ds: CramerGANDataset,
            weighted: bool = True,
            n_critic: int = 15):

        for epoch in range(epochs):
            start = time.time()

            # Train
            with tqdm(total=train_ds.ds_size) as pbar:
                for step, batch in enumerate(train_ds.dataset):
                    ds1, ds2, ds3 = batch

                    feat1, target1, w1 = ds1
                    feat2, target2, w2 = ds2
                    feat3, target3, w3 = ds3

                    for i in range(n_critic):
                        features = (feat1[i], feat2[i], feat3[i])
                        targets = (target1[i], target2[i], target3[i])
                        weights = (w1[i], w2[i], w3[i]) if weighted else (1., 1., 1.)

                        disc_loss = self.train_step_critic(
                            step, features, targets, weights)

                    features = (feat1[-1], feat2[-1], feat3[-1])
                    targets = (target1[-1], target2[-1], target3[-1])
                    weights = (w1[-1], w2[-1], w3[-1]) if weighted else (1., 1., 1.)

                    gen_loss, reg_loss = self.train_step_generator(
                        step, features, targets, weights)

                    pbar.set_postfix({
                        'gen_loss': gen_loss.numpy().squeeze(),
                        'disc_loss': disc_loss.numpy().squeeze(),
                        'reg_loss': reg_loss.numpy().squeeze()
                    })
                    pbar.update(train_ds.total_batch)

            # saving (checkpoint) the model every 20 epochs
            if (epoch + 1) % 20 == 0:
                # TODO: replace with checkpoint manager
                self.checkpoint.save(file_prefix=self.checkpoint_prefix)

            print('Time taken for epoch {} is {} sec\n'.format(
                epoch + 1, time.time()-start))

            self.evaluate(test_ds)

        self.checkpoint.save(file_prefix=self.checkpoint_prefix)

    def restore(self, restore_path):
        self.checkpoint.restore(restore_path)

    def restore_last(self):
        last_ckpt = tf.train.latest_checkpoint(self.checkpoint_dir)
        print("Last ckpt: ", last_ckpt)

        self.checkpoint.restore(last_ckpt)
