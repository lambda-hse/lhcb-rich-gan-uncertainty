Links to colab notebooks:

1. Efficiency: https://colab.research.google.com/drive/17dgxuwoPp4TARt4Sm-wwCWYIihobySTy
2. Efficiency (extrapolation scan): https://colab.research.google.com/drive/14JYcC35HPisj57JAwgogmE1Rq6icqtxO
3. GAN training: https://colab.research.google.com/drive/1N3mZw8S-pJbNUEN_K3eoqNZmvwOxPgbf?usp=sharing
4. GAN training (extrapolation scan): https://colab.research.google.com/drive/1ZNdtvZAp3hlaH5o8PdWDQNTGI6fKJWFl?usp=sharing